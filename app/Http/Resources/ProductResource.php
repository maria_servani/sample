<?php

namespace App\Http\Resources;

use App\Models\AboutUs;
use App\Models\Organization;
use App\Models\OrganizationGroupType;
use App\Models\OrganizationMentor;
use App\Traits\OrganizationHistoryTrait;
use App\Traits\UserSurveyTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id ,
            'slug' => $this->slug ,
            'title' => $this->title ,
            'owner' => [
                'name' => $this->owner->name,
                'family' => $this->owner->family,
            ] ,
            'cost' => $this->cost,
            'created_date' => $this->created_at,
        ];
    }
}
