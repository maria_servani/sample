<?php

namespace App\Http\Resources;

use App\Models\AboutUs;
use App\Models\Organization;
use App\Models\OrganizationGroupType;
use App\Models\OrganizationMentor;
use App\Traits\OrganizationHistoryTrait;
use App\Traits\UserSurveyTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id ,
            'full_name' => $this->FullName() ,
            'name' => $this->name ,
            'family' => $this->family ,
            'email' => $this->email ,
            'status' => $this->status ,
            'city' => $this->city ? $this->city->name  : null ,
            'products' => $this->products(),
            'created_date' => $this->created_at,
        ];
    }
}
