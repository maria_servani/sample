<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::FullRelation()->paginate();

        $productsRes = ProductResource::collection($products);

        return ['products' => $productsRes];
    }

    public function show(Product $product)
    {
        return ['product' => new ProductResource($product)];

    }

    public function create(ProductRequest $request)
    {

        $validated = $request->validated();
        if (!isset($validated['slug'])) 
                $validated['slug'] = Product::randomSlug();

        $product = Product::create([...$validated + ['user_id' => auth()->id()]]);

        return ['product' => new ProductResource($product)];
    }

    public function update(ProductRequest $request, Product $product)
    {
        $this->authorize('update' , $product);

        $validated = $request->validated();
        $product->update($validated);
        return ['product' => new ProductResource($product)];

    }

    public function delete(Product $product)
    {
        $this->authorize('delete' , $product);

        $product->delete();
        return response(['message' => 'Product deleted successfully']);
    }
}
