<?php

namespace App\Http\Controllers\User;

use App\Facade\MediaService;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::FullRelation()->paginate();

        $usersRes = UserResource::collection($users);

        return ['users' => $usersRes];
    }

    public function show(User $user)
    {
        return ['user' => new UserResource($user)];

    }

    public function create(UserRequest $request)
    {
        try {
            $validate = $request->validated(); 
            $user = User::create($validate);
            MediaService::upload($validate['avatar'], $user);
       } catch (\Throwable $th) {
           throw $th;
        }

        return ['user' => new UserResource($user)];
    }

    public function update(UserRequest $request, User $user)
    {
        $validate = $request->validated();
        $user->update($validate);
        return ['user' => new UserResource($user)];

    }

    public function delete(User $user)
    {
        $user->delete();
        return response(['message' => 'User deleted successfully']);
    }
}
