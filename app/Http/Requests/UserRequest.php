<?php

namespace App\Http\Requests;

use App\Enums\UserStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'email' => ['required','string','email',Rule::unique('users')],
            'name' => 'required|string',
            'family' => 'required|string',
            'password' => 'required|string|min:8|confirmed',
            'status' => ['required','string',new Enum(UserStatus::class)],
            'avatar' => 'required|image',
            'city_id' => 'required|exists:cities,id',
        ];
    }
}
