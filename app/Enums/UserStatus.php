<?php
namespace App\Enums;


enum UserStatus:string
{
    case ACTIVE = 'active';
    case NOTACTIVE = 'not-active';

}