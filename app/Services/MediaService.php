<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class MediaService
{
    public function upload($avatar, $user)
    {
        try {
            $file = $avatar;
            $name = $file->hashName().'.'.$file->getClientOriginalExtension();
            $upload = Storage::put("avatars/{$user->id}/{$name}", $file);
            $user->update(['avatar' => $name]);
        } catch (\Throwable $th) {
            throw $th;
        }
        return true;
    }
}