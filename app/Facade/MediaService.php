<?php

namespace App\Facade;

use Illuminate\Support\Facades\Facade;

class MediaService extends Facade
{
    protected static function getFacadeAccessor()
    {
       return \App\Services\MediaService::class ;
    }
}
