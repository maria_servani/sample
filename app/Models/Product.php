<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'title',
        'cost',
        'slug',
        'user_id',
    ];

    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public static function randomSlug()
    {
        do {
            $randomSlug = Str::random(30);
        } while (Product::where('slug', $randomSlug)->exists());
        return $randomSlug;
    }

    static function FullRelation()
    {
        return self::with([
            'owner'
        ]);
    }
}
