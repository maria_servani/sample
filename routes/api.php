<?php

use App\Http\Controllers\Product\ProductController;
use App\Http\Controllers\User\AuthController;
use App\Http\Controllers\User\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
 */
Route::group(['prefix' => 'v1'], function () {

    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', [AuthController::class, 'login']);

        Route::group(['middleware' => 'auth:sanctum'], function () {
            Route::get('logout', [AuthController::class, 'logout']);
        });
    });

    Route::group(['prefix' => 'users', 'middleware' => 'auth:sanctum'], function () {
        Route::get('/', [UserController::class, 'index']);
        Route::get('/show/{user}', [UserController::class, 'show']);
        Route::post('/create', [UserController::class, 'create']);
        Route::post('/update/{user}', [UserController::class, 'update']);
        Route::post('/delete/{user}', [UserController::class, 'delete']);
    });

    Route::group(['prefix' => 'products', 'middleware' => 'auth:sanctum'], function () {
        Route::get('/', [ProductController::class, 'index']);
        Route::get('/show/{product}', [ProductController::class, 'show']);
        Route::post('/create', [ProductController::class, 'create']);
        Route::post('/update/{product}', [ProductController::class, 'update']);
        Route::post('/delete/{product}', [ProductController::class, 'delete']);
    });
});
